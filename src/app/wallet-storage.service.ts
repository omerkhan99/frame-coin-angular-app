import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class WalletStorageService {

  constructor(@Inject(PLATFORM_ID) private platformId: Object) { }

  private isServiceAvailable(){

    return isPlatformBrowser(this.platformId);
  }

  private isDuplicatePassword(password){

    let storage = localStorage || window.localStorage;
    if(storage){

      return !!storage.getItem(password);
    }
    else{

      throw new Error('Local storage not available');
    }
  }

  saveWallet(password, pvtKey){

    if( !this.isServiceAvailable() )
      throw new Error('Local storage not available');
    if( this.isDuplicatePassword(password) )
      throw new Error('Another wallet is also using same password');
    
    let storage = localStorage || window.localStorage;
    if(storage){

      storage.setItem(password, pvtKey);
      return pvtKey;
    }
    else{

      throw new Error('Local storage not available');
    }
  }

  getWallet(password){

    if( !this.isServiceAvailable() )
      throw new Error('Local storage not available');
    if( !this.isDuplicatePassword(password) )
      throw new Error('No wallet exists against this password');

    let storage = localStorage || window.localStorage;
    if(storage){

      return storage.getItem(password);
    }
    else{

      throw new Error('Local storage not available');
    }
  }
}
