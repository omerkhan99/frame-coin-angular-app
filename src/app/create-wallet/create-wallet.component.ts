import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../web3.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WalletStorageService } from '../wallet-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-wallet',
  templateUrl: './create-wallet.component.html',
  styleUrls: ['./create-wallet.component.css']
})
export class CreateWalletComponent implements OnInit {

  password: string;

  constructor(
    private walletSrvc: Web3Service,
    private matSnackBar: MatSnackBar,
    private walletStorageSrvc: WalletStorageService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  // Generates new wallet and logs into it.
  generateWallet(){

    if(this.password.trim().length < 6){

      this.matSnackBar.open('Password must be atleast 6 characters', 'OK', { duration: 5000 });
    }
    else{

      this.walletSrvc.generateNew()
        .then((keys)=>{

          var privateKey = keys['private'];
          this.walletStorageSrvc.saveWallet(this.password, privateKey);
          return privateKey;
        })
        .then((pvtKey)=>{

          this.mountWallet(pvtKey);
        })
        .catch((err)=>{

          this.matSnackBar.open(err.message, 'OK');
        })
      ;

      
    }
  }

  // Mounts wallet using private key.
  mountWallet(pvtKey){

    var wallet = this.walletSrvc.setWallet( pvtKey );
    if(wallet instanceof Error){

      this.matSnackBar.open(wallet.message, 'OK', { duration: 5000 });
    }
    else{

      this.walletSrvc.updateBalance()
        .then(()=>{

          this.router.navigate(['wallet']);
        })
        .catch((err)=>{

          this.matSnackBar.open(err.message, 'OK', { duration: 5000 });
          this.router.navigate(['mount']);
        })
      ;
    }
    
  }
}
