import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../web3.service';
import { Wallet } from '../wallet';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ngCopy } from 'angular-6-clipboard';

@Component({
  selector: 'app-wallet-details',
  templateUrl: './wallet-details.component.html',
  styleUrls: ['./wallet-details.component.css']
})
export class WalletDetailsComponent implements OnInit {

  wallet: Wallet;

  constructor(
    private walletSrvc: Web3Service,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) { }

  ngOnInit() {

    this.wallet = this.walletSrvc.getWallet();
    if(!this.wallet){

      this.router.navigate(['mount']);
    }
  }

  copyAddress(){

    ngCopy(this.wallet.getAddressString());
    this.matSnackBar.open('Adress copied', 'OK', { duration: 3000 });
  }

  copyPrivateKey(){

    ngCopy(this.wallet.getPrivateKeyString());
    this.matSnackBar.open('Private key copied', 'OK', { duration: 3000 });
  }

}
