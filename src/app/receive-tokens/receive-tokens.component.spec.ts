import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveTokensComponent } from './receive-tokens.component';

describe('ReceiveTokensComponent', () => {
  let component: ReceiveTokensComponent;
  let fixture: ComponentFixture<ReceiveTokensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiveTokensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
