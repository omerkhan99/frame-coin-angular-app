import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../web3.service';
import { Wallet } from '../wallet';
import { ngCopy } from 'angular-6-clipboard';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-receive-tokens',
  templateUrl: './receive-tokens.component.html',
  styleUrls: ['./receive-tokens.component.css']
})
export class ReceiveTokensComponent implements OnInit {

  wallet: Wallet;

  constructor(private walletSrvc: Web3Service, private matSnackBar: MatSnackBar) { }

  ngOnInit() {

    this.wallet = this.walletSrvc.getWallet();
  }

  // Copies wallet address to clipboard.
  copyAddress(){

    ngCopy(this.wallet.getAddressString());
    this.matSnackBar.open('Copied', 'OK', {

      duration: 3000
    });
  }

}
