import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../web3.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-send-tokens',
  templateUrl: './send-tokens.component.html',
  styleUrls: ['./send-tokens.component.css']
})
export class SendTokensComponent implements OnInit {

  toAddress;
  amountToSend;
  currency;

  constructor(
    private walletSrvc: Web3Service,
    private matSnackBar: MatSnackBar
  ) {

    this.toAddress = '';
    this.amountToSend = 0;
    this.currency = 'frm';
  }

  ngOnInit() {
  }

  // Transfers tokens or coins to receiving address.
  sendTokens(){

    this.matSnackBar.open('Sending...');

    let promise = null;

    if(this.currency === 'frm')
      promise = this.walletSrvc.transferTokens(this.toAddress, this.amountToSend);
    else if( this.currency === 'eth' )
      promise = this.walletSrvc.transferEth(this.toAddress, this.amountToSend);

    promise
      .then(()=>{

        this.matSnackBar.open('Successfully sent.', 'OK', { duration: 3000 });
        this.toAddress = '';
        this.amountToSend = 0;
      })
      .catch((err)=>{

        this.matSnackBar.open(err.message, 'err', { duration: 5000 });
      })
    ;
  }

}
