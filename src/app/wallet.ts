import { toBuffer, publicToAddress, privateToPublic, bufferToHex, addHexPrefix } from 'ethereumjs-util';

export class Wallet {

    privateKey;
    publicKey;
    address;
    balance;
    ethBalance;

    constructor(pvtKey){

        // Prepend 0x to get a hex buffer.
        this.privateKey = toBuffer(addHexPrefix(pvtKey));
        this.publicKey = privateToPublic(this.privateKey);
        this.address = publicToAddress(this.publicKey, false);
    }

    getPrivateKey(){

        return this.privateKey;
    }

    getPublicKey(){

        return this.publicKey;
    }

    getBalance(){

        return this.balance;
    }

    updateBalance(newBalance){

        this.balance = newBalance;
    }

    getEthBalance(){

        return this.ethBalance;
    }

    updateEthBalance(newBalance){

        this.ethBalance = newBalance;
    }

    getPrivateKeyString(){

        return bufferToHex(this.privateKey);
    }

    getPublicKeyString(){

        return bufferToHex(this.publicKey);
    }

    getAddress(){

        return this.address;
    }

    getAddressString(){

        return bufferToHex(this.address);
    }
}
