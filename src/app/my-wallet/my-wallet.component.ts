import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../web3.service';
import { Wallet } from '../wallet';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-my-wallet',
  templateUrl: './my-wallet.component.html',
  styleUrls: ['./my-wallet.component.css']
})
export class MyWalletComponent implements OnInit {

  wallet: Wallet;
  toAddress;
  amountToSend;

  constructor(
    private walletSrvc: Web3Service,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) { }

  // Gets reference of unlocked wallet or routes back to locked screen.
  ngOnInit() {

    this.wallet = this.walletSrvc.getWallet();
    if(!this.wallet){

      this.router.navigate(['mount']);
    }
  }

  // Refreshes wallet balances. eth and frm balances.
  refreshWallet(){

    this.matSnackBar.open('Updating wallet...');

    this.walletSrvc.updateBalance()
      .then(()=>{

        this.matSnackBar.open('Up to date!', 'OK', { duration: 3000 });
      })
      .catch((err)=>{

        this.matSnackBar.open(err.message, 'OK', { duration: 3000 });
      })
    ;
  }
}
