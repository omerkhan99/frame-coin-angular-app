import { Component, OnInit } from '@angular/core';
import { Web3Service } from './web3.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Frame Coin';
  sideBarSettings;

  // Sidebar menues based on current route.
  private sideBarSettingsAll = {

    'default': {

      back: false,
      logout: false,
      wallet: false,
      keys: false
    },
    '/mount': {

      back: false,
      logout: false,
      wallet: false,
      keys: false
    },
    '/create': {

      back: true,
      logout: false,
      wallet: false,
      keys: false
    },
    '/wallet/history': {

      logout: true,
      wallet: true,
      back: false,
      keys: true
    },
    '/wallet/send': {

      logout: true,
      wallet: true,
      back: false,
      keys: true
    },
    '/wallet/receive': {

      logout: true,
      wallet: true,
      back: false,
      keys: true
    },
    '/keys': {

      logout: true,
      wallet: true,
      back: false,
      keys: true
    }
  };

  constructor( private web3Srvc: Web3Service, private router: Router ){

    this.sideBarSettings = this.sideBarSettingsAll['default'];
  }

  ngOnInit(){

    this.router.events.subscribe((e)=>{

      if(e instanceof NavigationEnd){

        var path = e.urlAfterRedirects;
        this.sideBarSettings = this.sideBarSettingsAll[path] || this.sideBarSettingsAll['default'];
      }
    });
  }

  unmountWallet(){

    this.web3Srvc.setWallet(null);
    this.router.navigate(['mount']);
  }
}
