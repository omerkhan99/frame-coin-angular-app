import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule, Routes } from '@angular/router';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Web3Service } from './web3.service';


import { AppComponent } from './app.component';
import { MountWalletComponent } from './mount-wallet/mount-wallet.component';
import { CreateWalletComponent } from './create-wallet/create-wallet.component';
import { MyWalletComponent } from './my-wallet/my-wallet.component';
import { SendTokensComponent } from './send-tokens/send-tokens.component';
import { ReceiveTokensComponent } from './receive-tokens/receive-tokens.component';
import { TransactionsHistoryComponent } from './transactions-history/transactions-history.component';
import { WalletDetailsComponent } from './wallet-details/wallet-details.component';

const appRoutes: Routes = [

  { path: 'create', component: CreateWalletComponent },
  { path: 'mount', component: MountWalletComponent, data: {a: 1} },
  {
    path: 'wallet',
    component: MyWalletComponent,
    children: [

      { path: 'send', component: SendTokensComponent },
      { path: 'receive', component: ReceiveTokensComponent },
      { path: 'history', component: TransactionsHistoryComponent },
      { path: '**', redirectTo: 'history', pathMatch: 'full' }
    ]
  },
  { path: 'keys', component: WalletDetailsComponent },
  { path: '**', redirectTo: 'mount', pathMatch: 'full' }
];


@NgModule({
  declarations: [
    AppComponent,
    MountWalletComponent,
    CreateWalletComponent,
    MyWalletComponent,
    SendTokensComponent,
    ReceiveTokensComponent,
    TransactionsHistoryComponent,
    WalletDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlexLayoutModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MatSnackBarModule
  ],
  providers: [
    Web3Service
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
