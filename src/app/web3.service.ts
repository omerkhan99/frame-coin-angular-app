import { Injectable } from '@angular/core';
import Web3 from 'web3';
import * as contractMeta from './token-contract.abi';
import { Wallet } from './wallet';
import { toBuffer, privateToAddress, bufferToHex, isValidPrivate, isValidAddress, addHexPrefix } from 'ethereumjs-util';

@Injectable()
export class Web3Service {

  web3;
  contract;
  wallet: Wallet;
  contractAddr = '0x592d7bf353c7b0ff72e3a4b70ea391219d210ce3';

  constructor() {

    this.web3 = new Web3(Web3.givenProvider || 'http://169.60.157.140:4321');
  }

  generateNew(){

    return new Promise((resolve, reject)=>{

      let pvtKey = null;
      // Because of faulty randomHex implementation use do-while
      do{
        pvtKey = this.web3.utils.randomHex(32);
      }while(pvtKey.length != 66);

      let addressBuff = privateToAddress(toBuffer(pvtKey));

      resolve({
        private: pvtKey.substring(2),
        address: bufferToHex(addressBuff)
      });
      
    });
  }

  setWallet(privateKey){

    if(!isValidPrivate(toBuffer(addHexPrefix(privateKey)))){

      return new Error('Invalid private key');
    }
    else{

      this.wallet = new Wallet(privateKey);
      this.contract = new this.web3.eth.Contract(contractMeta.abi, this.contractAddr, { from: this.wallet.getAddressString() });
      return true;
    }
  }

  getWallet(){

    return this.wallet;
  }

  updateBalance(){

    let transaction = this.contract.methods.balanceOf(this.wallet.getAddressString());
    return transaction.call()
      .then((balance)=>{

        this.wallet.updateBalance(balance);
        return this.web3.eth.getBalance(this.wallet.getAddressString());
      })
      .then((ethBalance)=>{

        this.wallet.updateEthBalance(this.web3.utils.fromWei(ethBalance, 'ether'));
      })
    ;
  }

  transferEth(toAddr, value){

    return new Promise((resolve, reject)=>{

      try{

        if(!isValidAddress(toAddr))
          throw new Error('Invalid wallet address');
        if(typeof value !== 'number' || value <= 0)
          throw new Error('Invalid amount');

        this.web3.eth.sendTransaction({

          from: this.wallet.getAddressString(),
          to: toAddr,
          value: this.web3.utils.toWei(''+value, 'ether')
        }).then(()=>{

          return this.updateBalance();
        }).then((data)=>{

          resolve(data);
        }).catch((err)=>{

          reject(err);
        });
      }
      catch(err){

        reject(err);
      }
    });
  }

  transferTokens(toAddr, value){

    return new Promise((resolve, reject)=>{

      try{

        if(!isValidAddress(toAddr))
          throw new Error('Invalid wallet address');
        if(typeof value !== 'number' || value <= 0)
          throw new Error('Invalid amount');

        let transaction = this.contract.methods.transfer(toAddr, value);
        transaction.send({from: this.wallet.getAddressString()})
          .then(()=>{

            console.log('huwa');
            return this.updateBalance();
          })
          .then((data)=>{

            resolve(data);
          })
          .catch((err)=>{

            console.log(err);
            reject(err);
          })
        ;
      }
      catch(err){

        reject(err);
      }
    });

  }

}
