import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Web3Service } from '../web3.service';
import { WalletStorageService } from '../wallet-storage.service';
import { Wallet } from '../wallet';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-mount-wallet',
  templateUrl: './mount-wallet.component.html',
  styleUrls: ['./mount-wallet.component.css']
})
export class MountWalletComponent {

  privateKey: string;
  password: string;
  usePrivateKey: boolean;

  constructor(
    private router: Router,
    private web3Srvc: Web3Service,
    private matSnackBar: MatSnackBar,
    private walletStorageSrvc: WalletStorageService
  ) {

    this.password = '';
    this.privateKey = '';
    this.usePrivateKey = false;
  }

  // Unlocks wallet using private key.
  mountWallet(privateKey){

    var wallet = this.web3Srvc.setWallet( privateKey );
    if(wallet instanceof Error){

      this.matSnackBar.open(wallet.message, 'OK', { duration: 5000 });
    }
    else{

      this.web3Srvc.updateBalance()
        .then(()=>{

          this.router.navigate(['wallet']);
        })
        .catch((err)=>{

          this.matSnackBar.open(err.message, 'OK', { duration: 5000 });
          this.router.navigate(['mount']);
        })
      ;
    }
    
  }

  // Unlocks wallet using password.
  mountWithPassword(password){

    if(password.trim().length < 6){

      this.matSnackBar.open('Invalid Password', 'OK', { duration: 5000 });
    }
    else{

      try{

        let pvtKey = this.walletStorageSrvc.getWallet(password);
        this.mountWallet(pvtKey);
      }
      catch(err){

        this.matSnackBar.open(err.message, 'OK', { duration: 5000 });
      }
    }
  }
}
