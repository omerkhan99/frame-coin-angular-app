import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MountWalletComponent } from './mount-wallet.component';

describe('MountWalletComponent', () => {
  let component: MountWalletComponent;
  let fixture: ComponentFixture<MountWalletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MountWalletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MountWalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
